// CRUD Operarios
    // CRUD operation is the heart of any backend application.
    // mastering the CRUD operations is essential for any developer.
    // this helps in building character and increasing exposure to logical statements that will help us manipulate 
    // mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.

// [SECTION] Create (Inserting document);
    // Since mongoDB deals with object as it's structure for documents, we can easily create them by providing objects into our methods.
    // mongoDB shell aslo uses javascript for it's javascript for it's syntax which makes it convenient for us to understand it's code.

    // Insert one document
    /* 
        Syntax:
            db.collectionName.insertOne({object/document})
    */

            db.users.insertOne({
                firstName : "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "1234567890",
                    email: "janedoe@gmail.com"
                },
                courses: ["CSS", "Javascript", "Python"],
                department: "none"
            });

    // Insert many
    /*
        Syntax:
            db.users.insertMany ([{objectA},
            {objectB}])
    */ 

            db.users.insertMany([{
                firstName: "Stephen",
                lastName:"Hawking",
                age: 76,
                contact: {
                    phone: "87654321",
                    email: "stephenhawking@gmail.com"
                },
                courses: ["Python", "React", "PHP"],
                department: "none"
            }, {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "87654321",
                    email: "neilarmstrong@gmail.com"
                },
                courses: ["React", "Laravel", "Sass"],
                department: "none"    

            }]);

            db.users.insertOne({
                firstName : "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "1234567890",
                    email: "janedoe@gmail.com"
                },
                courses: ["CSS", "Javascript", "Python"],
            });

            db.users.insertOne({
                
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "1234567890",
                    email: "janedoe@gmail.com"
                },
                courses: ["CSS", "Javascript", "Python"],
                firstName : "Jane"
            });

            db.users.insertOne({
                firstName : "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "1234567890",
                    email: "janedoe@gmail.com"
                },
                courses: ["CSS", "Javascript", "Python"],
                gender: "Female"
            });

            db.users.insertOne({
                firstName : "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "1234567890",
                    phone: "123456789011",
                    email: "janedoe@gmail.com"
                },
                courses: ["CSS", "Javascript", "Python"],
                gender: "Female"
            });

// [SECTION] Find (read)
    // case sensitive
    /*
        Syntax:
            db.collectionName.find({fieldA:valueA, fieldB: valueB})

    */

    db.users.find();    //lahat ng docs lalabas

    db.users.find({age: 76});

    db.users.find({firstName: "jane"}); // string is case sensitive

    db.users.find({firstName: "Jane"});

    // finding documents using multiple fieldsets
    /*
            Syntax:
                db.collectionName.find({
                    fieldA: valueA,
                    fieldB: valueB
                })
    */

    db.users.find({lastName: "Armstrong", age: 82});

    db.users.find({lastName: "Armstrong", age: 83});



// [SECTION] Updating Documents

    // add document
    db.users.insertOne({
        firstName: "Test",
        lastName: "Test",
        age: 0,
        contact: {
            phone: "000000",
            email: "test@gmail.com"
        },
        course: [],
        department: "none"
    });

        // updateOne
            /*
                Syntax:
                    db.collectionName.updateOne({criteria},
                        $set: {field:value}});
            */

        db.users.updateOne({
            firstName: "Jane"
        }, {
            $set: {
                lastName: "Hawking"
            }
        });

        // updating multiple documents
        /*
            Syntax:
                db.collectionName.updateMany({criteria}, {$set: {
                    field:value
                }})
        */

        db.users.updateMany({firstName: "Jane"},{$set:{
            lastName: "Wick",
            age:25
        }})

        db.users.updateMany({firstName: "Jane"},{$set:{
            lastName: "Wick",
            age:25
        }})

        db.users.updateMany({department: "none"},{$set:{department: "HR"}})

        // replacing the old document
        /*
            syntax:
                db.users.replaceOne({criteria}, {document/objectToReplace})
        */

            db.users.replaceOne({firstName: "Test"},{
            firstName: "Chris",
            lastName: "Mortel"
        })
        
// [SECTION] Deleting documents
   

    // Deleting SIngle document
        /*
            Syntax:
                db.collectionName.deleteOne({criteria});
        */

        db.users.deleteOne({firstName: "Jane"});

    // Delete Many
        /*
            Syntax: 
                db.collectionName.deleteOne({criteria});
        */

        db.users.deleteMany({firstName: "Jane"});

    // reminder
        //db.collectionName.deleteMany()
        // NEVER FORGET TO ADD CRITERIA - it will delete all ur files

        db.users.deleteMany({});

// [SECTION] Advanced Queries
    // embedded - object
    // nested field - array

    // query on an embedded document
        // naka object ung format so may double quote
         db.users.find({"contact.phone": "87654321"});   

    // query an array with exact element
        // order should be the same
        db.users.find({courses: ["React", "Laravel", "Sass"]}); 

    // querying an array without regards to order and elemnts
        db.users.find({course:{$all: ["React"]}});




    
